
import {Col, Row, Card} from "react-bootstrap"

export default function Highlights(){
    return(
        <Row className="my-3">
            {/*This is the first card*/}
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card.Title><h2>Learn From Home</h2></Card.Title>
                    <Card.Text>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae pharetra magna. Phasellus condimentum libero eget neque pretium, at suscipit leo porta. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam hendrerit dapibus diam, non bibendum dui lobortis a. Sed consequat sodales ipsum. Sed malesuada ullamcorper lorem, ac aliquam augue laoreet sit amet. Praesent vitae orci dolor. In lobortis quis turpis et eleifend. Praesent eu accumsan eros. Phasellus nec mi ante. Ut quis libero mi. Cras dignissim porta ex, eu porttitor odio hendrerit sit amet. In hac habitasse platea dictumst. Cras eget imperdiet orci, sit amet venenatis lectus.
                    </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            {/*This is the second card*/}
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card.Title><h2>Study Now, Pay Later</h2></Card.Title>
                    <Card.Text>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae pharetra magna. Phasellus condimentum libero eget neque pretium, at suscipit leo porta. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam hendrerit dapibus diam, non bibendum dui lobortis a. Sed consequat sodales ipsum. Sed malesuada ullamcorper lorem, ac aliquam augue laoreet sit amet. Praesent vitae orci dolor. In lobortis quis turpis et eleifend. Praesent eu accumsan eros. Phasellus nec mi ante. Ut quis libero mi. Cras dignissim porta ex, eu porttitor odio hendrerit sit amet. In hac habitasse platea dictumst. Cras eget imperdiet orci, sit amet venenatis lectus.
                    </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            {/*This is the third card*/}
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card.Title><h2>Be part of our community</h2></Card.Title>
                    <Card.Text>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae pharetra magna. Phasellus condimentum libero eget neque pretium, at suscipit leo porta. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam hendrerit dapibus diam, non bibendum dui lobortis a. Sed consequat sodales ipsum. Sed malesuada ullamcorper lorem, ac aliquam augue laoreet sit amet. Praesent vitae orci dolor. In lobortis quis turpis et eleifend. Praesent eu accumsan eros. Phasellus nec mi ante. Ut quis libero mi. Cras dignissim porta ex, eu porttitor odio hendrerit sit amet. In hac habitasse platea dictumst. Cras eget imperdiet orci, sit amet venenatis lectus.
                    </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        
    )

}
