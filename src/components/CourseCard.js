import {useState,useEffect,useContext} from 'react';
import {Row,Col,Card,Button} from 'react-bootstrap';

import UserContext from '../UserContext';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {
    // console.log(props);

    // destructuring the courseProp that were passed from the Courses.js
    const {_id,name,description,price,slots}=courseProp

    // Use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
    // States are used to keep track information related to individual components
    // Syntax:
        // const[getter,setter]=useState(initialGetterValue)

    const [enrollees,setEnrollees]=useState(0);
    const [slotsAvailable,setSlotsAvailable]=useState(slots);
    
    const [isAvailable,setIsAvailable]=useState(true);

    const{user}=useContext(UserContext);

    // Add an "useEffect" hook to have "CourseCard" component to perform a certain task after every DOM update

        // Syntax:useEffect(functionToBeTriggered,[statesToBeMonitored])
    useEffect(()=>{
        if(slotsAvailable === 0){
            setIsAvailable(false);
        }
        
    },[slotsAvailable]);

    /*console.log(enrollees);*/

    /*setEnrollees(1);
    console.log(enrollees);*/

    function enroll() {
        if (slotsAvailable === 1 ) {
            alert("Congratulations,you were able to enroll brfore the cut!");
       
        } 
          setEnrollees(enrollees + 1);
         setSlotsAvailable(slotsAvailable - 1);
        }
    

    // since enrollees is declered as a constant variable,directly reassigning the value is not allowed or will cause an error

    return (
        <Row>
            <Col xs={12} md={4} className="offset-md-4 offset=0">
        <Card className="my-3">
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                   Php {price}
                </Card.Text>
                <Card.Subtitle>
                    Enrollees:
                </Card.Subtitle>
                <Card.Text>
                   {enrollees}
                </Card.Text>
                <Card.Subtitle>
                    Slots available:
                </Card.Subtitle>
                <Card.Text>
                   {slotsAvailable} slots
                </Card.Text>
                {
                    (user !==null)?
                    <Button as ={Link} to ={`/courses/${_id}`} variant="primary" 
                        disabled={!isAvailable}>Details</Button>
                        :
                        <Button as = {Link} to ="/login" variant="primary" 
                        disabled={!isAvailable}>Enroll</Button>
                }
                
            </Card.Body>
        </Card>
        </Col>
        </Row>
    )
}
