import { useEffect, useState } from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

export default function NoMatchPage(){
	const[time,setTime] = useState(5)
    const location = useNavigate()

    useEffect(() => {
        let timerInterval = setTimeout(() => setTime(time-1), 1000)

        if(time===0){
            location("/");
        }

        return () => {
            clearTimeout(timerInterval)
        }

    })

    return(
        <Container fluid>
        <Row className="text-center">
            <Col>
            <h1>
                You can't access this page.
            </h1>
            <p>
                Redirecting you to the homepage in {time} seconds...                
            </p>
            
            </Col>
           
            </Row>
    	</Container>
    )

}
