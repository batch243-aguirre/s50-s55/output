import {Container} from 'react-bootstrap'
/*import courseData from "../data/courses";*/
import CourseCard from "../components/CourseCard";
import {useEffect,useState} from 'react';

export default function Courses(){
	const [courses,setCourses]=useState([]);
	// console.log(courseData);

	// Syntax:
		// localstorage.getItem("propertyName");

/*	const local=localStorage.getItem("email")
	console.log(local);*/

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`)
		.then(response=>response.json())
		.then(data=>{
			console.log(data);
			// Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" Component

			setCourses(data.map(course=>{
		return(
			<CourseCard key={course._id} courseProp={course}/>
			)
			}))
		})
		
	},[]);

	return(
		<Container>
		{courses}
		</Container>
		)
}