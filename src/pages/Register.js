import {Container,Row,Col} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {useNavigate} from 'react-router-dom';
import Swal from "sweetalert2";
import {useState,useEffect,useContext} from 'react';

import	UserContext from '../UserContext';

export default function Register(){
	// State hooks to store the values of the input field of our user

	const [fName, setFName] = useState('');
    const [lName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
	const [password1,setPassword1]=useState('');
	const [password2,setPassword2]=useState('');
	const [isActive,setIsActive]=useState(false);

	const history = useNavigate();

	const {user} = useContext(UserContext);

	// Business logic
	// We want to disable the register button if one of the input fields is empty

	useEffect(() =>{


        if((fName !== '' && lName !=='' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !=='') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [fName, lName, email, mobileNo, password1, password2])


	/*this function will be triggered when the inputs in the Form will be submitted*/

		function registerUser(event) {
		  event.preventDefault();
		  fetch(`${process.env.REACT_APP_URI}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		      "Content-Type": "application/json",
		    },
		    body: JSON.stringify({
		      email: email,
		    }),
		  })
		    .then((res) => res.json())
		    .then((data) => {
		      console.log(data);

		      if (data.emailExists) {
		        Swal.fire({
		          title: "Duplicate email found",
		          icon: "error",
		          text: "Kindly provide another email to complete the registration.",
		        });
		      }else{

                fetch(`${process.env.REACT_APP_URI}/users/register`,{
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: fName,
                        lastName: lName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to Zuitt!"
                        });
                        setFName('');
                        setLName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');
                        history("/login");
                    }
                    else{

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please try again."
                        });

                    }
                })


            }
        })
    }





	return(
		
		<Container>
		<Row>
		<Col className="col-md-4 col-8 offset-md-4 offset-2">
		<Form onSubmit={registerUser} className='bg-secondary p-3'>
	      	
	      	<Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter first name"
                    value={fName}
                    onChange={event => setFName(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                    type="text"
                    placeholder="Enter last name"
                    value={lName}
                    onChange={event => setLName(event.target.value)}
                    required
                />
            </Form.Group>

	      <Form.Group className="mb-3" controlId="email">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        type="email" 
	        placeholder="Enter email" 
	        value={email}
	        onChange={event =>setEmail(event.target.value)}
	        required/>
	       
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="09xxxxxxxxx"
                    value={mobileNo}
                    onChange={event => setMobileNo(event.target.value)}
                    required
                />
            </Form.Group>


	        <Form.Label>Password</Form.Label>
	      <Form.Group className="mb-3" controlId="password1" >
	        <Form.Control 
	        type="password" 
	        placeholder="Password" 
	        value={password1}
	        onChange={event =>setPassword1(event.target.value)}
	        required/>
	      </Form.Group>


	        <Form.Label>Verify your Password</Form.Label>
	      <Form.Group className="mb-3" controlId="password2" >
	        <Form.Control 
	        type="password" 
	        placeholder="Password" 
	        value={password2}
	        onChange={event =>setPassword2(event.target.value)}
	        required />
	      </Form.Group>
      		

      		{
	  	isActive ?

	  	<Button variant="primary" type="submit">
	        Submit
	    </Button>

	    :
	    <Button variant="primary" type="submit" disabled>
	        Submit
	     </Button>

	  } 


    	</Form>
		</Col>
		</Row>
		</Container>
		)
}