
import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function	Logout(){
	const {unsetUser, setUser} = useContext(UserContext);
	unsetUser();
	useEffect(() => {
		setUser({id:null,isAdmin:false});

	});

	Swal.fire({
					title:"Welcome back to the outside World!",
					icon:"info",
					text:"Babalik ka rin!"
				})


	return(
		<Navigate to ='/login'/>
		)
}