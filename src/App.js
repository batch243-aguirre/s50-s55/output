import './App.css';

//pages import
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NoMatchPage from './pages/NoMatchPage';
import CourseView from './pages/CourseView';



/*import {Fragment} from 'react';*/
import {BrowserRouter as Router,Routes,Route} from 'react-router-dom'
import  {useState,useEffect} from 'react';

import {UserProvider} from './UserContext';

function App() {

  // State hook for the user that will be globally accessible using the useContext
  // This will also be used to store the user information and be used for validating if a user is logged in on the app or not

  const [user,setUser]=useState({id:null,isAdmin:false});

  // Function for clearing local storage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(()=>{
    console.log(user)
  },[user])

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_URI}/users/profile`,
        {headers:{
          Authorization:`Bearer ${localStorage.getItem('token')}`
        }})
        .then(response=>response.json())
        .then(data => {
        console.log(data);
        
        setUser({id:data._id,isAdmin: data.isAdmin})

        })
  },[])
  
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Routes>
        <Route path="/" element ={<Home/>}/>
        <Route path="/courses" element ={<Courses/>}/>
        <Route path="/courses/:courseId" element ={<CourseView/>}/>
        <Route path="/login" element ={<Login/>}/>
        <Route path="/register" element ={<Register/>}/>
        <Route path="/logout" element ={<Logout/>}/>
        <Route path="*" element ={<NoMatchPage/>}/>
      </Routes>
    </Router>
    </UserProvider>
      
    
  );
}

export default App;

